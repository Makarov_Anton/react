import React from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { InputGroup } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
const AUTHORS = {
  ME: 'Me',
  BOT: 'Bot'
}

function usePrevious(value) {
  const ref = React.useRef();
  React.useEffect(() => {
    ref.current = value;
  }, [value]);

  return ref.current;
}

function Messages(props) {
  return (
    <React.Fragment>
      <p>{props.author}: {props.text}</p>
    </React.Fragment>
  );
}

function App() {

  const [messagesList,setMessagesList] = React.useState([])
  const [inputValue,setInputValue] = React.useState("")
  const [id,setId] = React.useState(0)
  const prevMessagesList = usePrevious(messagesList)
  const timer = React.useRef(null)

  React.useEffect(() => {
    if (messagesList.length && prevMessagesList.length < messagesList.length &&
      messagesList[messagesList.length -1].author !== AUTHORS.BOT) {
      timer.current = setTimeout(() => {
        setMessagesList(cur => ([...cur,{ author: AUTHORS.BOT, text: "Привет"}]))
      },1500)
    }
  },[messagesList, prevMessagesList])

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current)
    }
  })

  const handleMessagesChange = (e) => {
    setInputValue(e.target.value)
  }
  const handleMessagesSubmit = (e) => {

    e.preventDefault()
    setMessagesList(cur => ([...cur,{id: id, author: AUTHORS.ME, text: inputValue}]))
    setInputValue("")
    setId(id + 1)

  }

  return (
    <div className="App">
      <header className="App-header">

        <Container>
          <Row>
            <Col md={{ span: 6, offset: 3 }}>
              {messagesList.length ? <div className="text-start border border border-4 border-primary">
                {messagesList.map((message) => (
                  <Messages key={message.id} id={message.id} author={message.author} text={message.text}/>
                ))}
              </div> : null}
              <Form onSubmit={handleMessagesSubmit}>
                <Form.Group className="mb-3" >
                  <InputGroup className="mb-3">
                    <FormControl
                      autoFocus
                      className="form-control"
                      placeholder="Введите сообщение"
                      value={inputValue}
                      onChange={handleMessagesChange}
                      required
                    />
                    <Button variant="primary" type="submit">Button</Button>
                  </InputGroup>
                </Form.Group>
              </Form>
            </Col>
          </Row>

        </Container>



      </header>
    </div>
  );
}

export default App;
