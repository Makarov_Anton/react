import React from 'react'
import './App.css';

class ExampleClass extends React.Component {
  render() {
    return <p className="example_border">{this.props.text}</p>
  }
}

function ExampleFunction(props) {
  return <p className="example_border">{props.text}</p>
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ExampleFunction text="Функция"/>
        <ExampleClass text="Класс"/>
      </header>
    </div>
  );
}

export default App;
