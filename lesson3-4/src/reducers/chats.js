import {ADD_NEW_CHAT} from "../actions/chats";
import {REMOVE_CHAT} from "../actions/chats";
import {CHANGE_CURRENT_CHAT} from "../actions/chats";

const initialState = {
  curChat: '3',
  chats: [
    {id: "0", name: "Первый"},
    {id: "1", name: "Второй"},
    {id: "2", name: "Третий"},
    {id: "3", name: "Четвёртый"},
    {id: "4", name: "Пятый"},
  ]}

function chatsReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_NEW_CHAT: {
      return {
        ...state,
        chats: [...state.chats, {id: String(Date.now()), name: action.payload.chats}]
      }
    }
    case REMOVE_CHAT: {
      return {
        ...state,
        chats: state.chats.filter((chat) => chat.id !== action.payload.chatId)
      }
    }
    case CHANGE_CURRENT_CHAT: {
      return {
        ...state,
        curChat: String(+action.payload.curChat)
      }
    }
    default:
      return state
  }
}

export default chatsReducer;