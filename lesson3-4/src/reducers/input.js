import {ADD_MESSAGE} from "../actions/input";

const initialState = {
  message: ''
}

function inputReducer(state = initialState,action) {
  switch (action.type) {
    case ADD_MESSAGE: {
      return { message: action.payload.message }
    }

    default:
      return state
  }

}

export default inputReducer;