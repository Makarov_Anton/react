import {AUTHORS} from "../components/Constants/Constants";
import {ADD_MESSAGE} from "../actions/messages";

const initialState = {
  '3': [
    {id: '1', author: AUTHORS.BOT, text: "Добро пожаловать, путник!"},
    {id: '2', author: AUTHORS.ME, text: "Здоровеньки!"},
  ],
  'default': [
    {id: '0', author: AUTHORS.BOT, text: "У вас нет сообщений с данным пользователем"},
  ]
}

function messagesReducer(state = initialState, action) {
  switch (action.type) {

    case ADD_MESSAGE: {
      return {
        ...state,
        [action.payload.chatId]: [
          ...state[action.payload.chatId] || [],
          action.payload.message
        ]
      }
    }

    default:
      return state
  }
}

export default messagesReducer;