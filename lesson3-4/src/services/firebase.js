import { initializeApp } from 'firebase/app';
import { getDatabase } from "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyDqcIseJytt3J7dcdvpfS5wbkqNGo32Be0",
  authDomain: "sector-54b5c.firebaseapp.com",
  databaseURL: "https://sector-54b5c-default-rtdb.firebaseio.com",
  projectId: "sector-54b5c",
  storageBucket: "sector-54b5c.appspot.com",
  messagingSenderId: "589073632987",
  appId: "1:589073632987:web:17314fcb3d5b4df77286fe",
  measurementId: "G-DMT0FKT1GD"
}

const app = initializeApp(firebaseConfig);

getDatabase(app);