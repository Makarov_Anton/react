export const ADD_MESSAGE = "INPUT::ADD_MESSAGE"

export const addMessage = (message) => ({
  type: ADD_MESSAGE,
  payload: {
    message: message,
  },
})
