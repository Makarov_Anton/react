import {AUTHORS} from "../components/Constants/Constants";
import { getDatabase, onValue ,ref, set } from "firebase/database";

export const ADD_MESSAGE = "MESSAGES::ADD_MESSAGE"

export const addMessage = (chatId, message) => ({
  type: ADD_MESSAGE,
  payload: {
    chatId: chatId,
    message: message,
  },
})

export const addMessageWithThunk = (chatId, message) => {
  return (dispatch, getState) => {
    const db = getDatabase();

    const messageObjMe = {id: String(Date.now()), author: AUTHORS.ME, text: message}
    const messageBot = 'Я глупый бот)'
    const messageObjBot = {id: String(Date.now()+1), author: AUTHORS.BOT, text: messageBot}
    const refMe = ref(db, 'chat' + chatId+'/messages' + String(Date.now()))

    set(refMe, messageObjMe);
    onValue(refMe, (snapshot) => {
      console.log(snapshot.val());
    });


    let timer = setTimeout(() => {
      // set(ref(db, 'chat' + chatId+'/messages' + String(Date.now())), messageObjBot);
      dispatch(addMessage(chatId, messageObjBot))
      clearTimeout(timer)
    }, 2000)

  }
}

export const subscribeOnMessagesChangings = (chatId) => {
  return (dispatch, getState) => {
    const db = getDatabase();
    const refMe = ref(db, String(chatId) + '/messages' + String(Date.now()))

    onValue(refMe, (snapshot) => {
      dispatch(addMessage(chatId, snapshot.val()))
    });
  }
}