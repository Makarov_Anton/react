export const ADD_NEW_CHAT = "CHAT::ADD_NEW_CHAT"
export const REMOVE_CHAT = "CHAT::REMOVE_CHAT_CHAT"
export const CHANGE_CURRENT_CHAT = "CHAT::CHANGE_CURRENT_CHAT"

export const addNewChat = (chats) => ({
  type: ADD_NEW_CHAT,
  payload: {
    chats: chats,
  },
})

export const removeChat = (chatId) => ({
  type: REMOVE_CHAT,
  payload: {
    chatId: chatId,
  },
})

export const changeCurChat = (curChat) => ({
  type: CHANGE_CURRENT_CHAT,
  payload: {
    curChat: curChat ,
  },
})