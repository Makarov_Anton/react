import React from 'react';
import {Route, Switch, Redirect } from "react-router-dom"
import ChatIdLink from "../ChatIdLink/ChatIdLink";
import ChatList from "../ChatList/ChatList";
import HomeContainer from '../Home/HomeContainer'
import News from "../News/News";
import ProfileContainer from "../profile/ProfileContainer";
import Login from "../Login";
import {useSelector} from "react-redux";
// import Profile from "../profile/profile";
// import Home from "../Home";

const PrivateRoute = (props) => {
  const isAuthed = useSelector(state => state.profile.isAuthed)
  return isAuthed ? <Route {...props} /> : <Redirect to="/login" />
}

export default function Router(props) {
  return (
    <div>
      <Switch>
        <Route
          path="/"
          exact
          render={() => (
            <React.Fragment>
              <p>Container</p>
              <HomeContainer />

              {/*<p>Component</p>*/}
              {/*<Home*/}
              {/*  age={12}*/}
              {/*  name={'Alice'}*/}
              {/*  onChangeProfileName={(name) => console.log(name)}*/}
              {/*/>*/}
            </React.Fragment>
          )}
        />


        <PrivateRoute path={'/chats'} exact render={() => <ChatList />} />

        <PrivateRoute path="/chats/:chatId" render={() => <ChatIdLink />} />

        <PrivateRoute
          path="/profile"
          exact
          render={() => (
            <React.Fragment>
              <p>Container</p>
              <ProfileContainer />

              {/*<p>Component</p>*/}
              {/*<Profile*/}
              {/*  age={12}*/}
              {/*  name={'Alice'}*/}
              {/*  isOnline={true}*/}
              {/*  onChangeProfileIsOnline={(isOnline) => console.log(isOnline)}*/}
              {/*/>*/}
            </React.Fragment>
          )}
        />
        <Route path={'/login'} exact render={() => <Login />} />

        <Route path={'/news'} exact render={() => <News />} />

        <Route>
          <p>404: not found</p>
        </Route>


        }}/>

      </Switch>

    </div>
  )
}