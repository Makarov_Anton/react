import React from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Router from "../Router/Router";
import {Container, Navbar, Nav, Row} from 'react-bootstrap';
import {signOut, getAuth, onAuthStateChanged} from "firebase/auth";
import {useDispatch} from "react-redux";
import {changeIsAuthed} from "../../actions/profile";
import {Link} from "react-router-dom";



export default function App() {
  const auth = getAuth()
  const dispatch = useDispatch()




  React.useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      dispatch(changeIsAuthed(!!user))
    })
  },[])

  const handleSignOut = (e) => {
    e.preventDefault()
    signOut(auth).then(() => {
    }).catch((error) => {
    });
  }

  return (
    <div className="App">
      <Navbar bg="light" variant="light">
        <Container>
          <Link to="/" className="navbar-brand">FirmaS</Link>
          <Nav className="me-auto">
            <Link to="/" className="nav-link">Home</Link>
            <Link to="/chats" className="nav-link">chats</Link>
            <Link to="/profile" className="nav-link">profile</Link>
            <Link to="/news" className="nav-link">news</Link>
            <Link to="/login" className="nav-link">login</Link>
            <a href="/" onClick={handleSignOut} className="nav-link">ВЫХОД</a>
          </Nav>
        </Container>
      </Navbar>
      <Container>
        <Row className="App-body">
          <Router/>
        </Row>
      </Container>
    </div>
  )
}

