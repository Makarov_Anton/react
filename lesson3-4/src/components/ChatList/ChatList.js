import React from 'react';
import {ListGroup, Badge, Col} from 'react-bootstrap';
import {useHistory} from 'react-router-dom'
import Input from "../Input/Input";
import {useDispatch, useSelector} from "react-redux";
import {addNewChat, removeChat, changeCurChat} from "../../actions/chats";


export default function ChatList() {

  const dispatch = useDispatch()
  const {curChat, chats} = useSelector(state => state.chats)

  const setCurChat = (newCurChat) => {
    dispatch(changeCurChat(newCurChat))
  }

  const addChats = (newChat) => {
    dispatch(addNewChat(newChat))
  }

  const removeCurChat = (chatId) => {
    dispatch(removeChat(chatId))
  }

  const onAddChat = newChatName => {
    addChats(newChatName)
  }

  const onRemoveChat = (e, chatId) => {
    e.stopPropagation()
    removeCurChat(chatId)
  }

  const history = useHistory()

  const handleChatLinkClick = chat => {
    setCurChat(chat)
    history.push(`/chats/${chat}`)
  }

  return (
    <>
      <Col md={{span: 4}}>
        <ListGroup as="ul" className="text-start border border border-primary d-flex justify-content-between">
          {chats.map((chat) => {
            return (
              <ListGroup.Item as="li" className="d-flex justify-content-between align-items-start" key={chat.id}
                              active={curChat === chat.id} onClick={() => handleChatLinkClick(chat.id)}>
                <div>{chat.id}: {chat.name}</div>
                <Badge className="bg-danger" onClick={(e) => onRemoveChat(e, chat.id)}>X</Badge>
              </ListGroup.Item>
            )

          })}
        </ListGroup>
        <Input onSubmit={onAddChat}/>
      </Col>
    </>
  );
}



