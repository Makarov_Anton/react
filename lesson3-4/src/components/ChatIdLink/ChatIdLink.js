import {Redirect, useParams} from "react-router-dom";
import React from "react";
import ChatForm from "../ChatForm/ChatForm";
import { useSelector} from "react-redux";

export default function ChatIdLink() {

  const {chatId} = useParams()
  const useIsChatExists = ({chatId}) => {
    const {chats} = useSelector(state => state.chats)
    return Boolean(Object.values(chats).find(chat => chat.id === chatId))
  }
const isChatExists = useIsChatExists({chatId})
  if (!isChatExists) {
    return <Redirect to="/chats"/>
  }

  return (
    <div>
      <ChatForm chatId = {chatId}/>
    </div>
  );
}

