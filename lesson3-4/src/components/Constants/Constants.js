export const AUTHORS = {
  ME: 'Me',
  BOT: 'Bot',
}

export const API_URL = 'https://api.spaceflightnewsapi.net/v3/articles'

export const NEWS_REQUEST_STATUS = {
  LOADING: 'loading',
  ERROR: 'error',
  IDLE: 'idle'
}