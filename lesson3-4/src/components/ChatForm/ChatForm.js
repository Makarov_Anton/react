import React from "react";
import Messages from "../Messages/Messages";
import Input from "../Input/Input";
import {useDispatch, useSelector} from "react-redux";
import {addMessageWithThunk, subscribeOnMessagesChangings} from "../../actions/messages";
// import { getDatabase } from "firebase/database";

export default function ChatForm(props) {
  // const db = getDatabase();
  const dispatch = useDispatch()
  const {chatId} = props;
  const messages = useSelector(state => state.messages[chatId] || state.messages['default'])

  const handleMessagesSubmit = (newMessageText) => {
    dispatch(addMessageWithThunk(chatId, newMessageText))
  }

  React.useEffect(() => {
    dispatch(subscribeOnMessagesChangings(chatId))
  }, [])

  return (
    <div>
      <h1>{ chatId }</h1>
      {messages.length ?
        <div className="text-start border border border-4 border-primary">
          {messages.map((message) => (
          <Messages key={message.id} id={message.id} author={message.author} text={message.text}/>
          ))}
        </div>
        : null}
      <Input onSubmit={handleMessagesSubmit}/>
    </div>
  )
}