import React from 'react'
import {getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword} from "firebase/auth";
import {Button, Form} from "react-bootstrap";

export default function Login(props) {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [error, setError] = React.useState('')
  const [isSigningUp, setIsSigningUp] = React.useState(false)

  const handleChangeEmail = (e) => setEmail(e.target.value)
  const handleChangePassword = (e) => setPassword(e.target.value)
  const handleIsSigningUpChange = (e) => setIsSigningUp(e.target.checked)
  const auth = getAuth()

  const handleLogin = async () => {
    try {
      await signInWithEmailAndPassword(auth, email, password)
    } catch (error) {
      setError(error.message)
    }
  }

  const handleSignUp = async () => {
    try {
      await createUserWithEmailAndPassword(auth, email, password)
    } catch (error) {
      setError(error.message)
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault()

    if (!email || !password) {
      setError('Заполните поля')
      return
    }

    if (isSigningUp) {
      handleSignUp()

    }else {
      handleLogin()
    }
  }

  return (
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" onChange={handleChangeEmail}/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" onChange={handleChangePassword}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Check me out" checked={isSigningUp}
                    onChange={handleIsSigningUpChange}/>
      </Form.Group>
      <Button variant="primary" type="submit" onClick={handleSubmit}>
        {isSigningUp ? 'Зарегистрироваться' : 'Войти'}
      </Button>
      <hr/>
      <p>{error}</p>
    </Form>
  )
}

