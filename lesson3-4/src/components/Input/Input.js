import {Button, Form, FormControl, InputGroup} from "react-bootstrap";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {addMessage} from "../../actions/input";

export default function Input(props) {
  const { onSubmit } = props
  const dispatch = useDispatch()
  const { message } = useSelector(state => state.input)

  const handleChange = (e) => {
    dispatch(addMessage(e.target.value))
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (onSubmit) {
      onSubmit(message)
      dispatch(addMessage(''))
    }
  }

  return  (
  <Form onSubmit={handleSubmit}>
    <Form.Group className="mb-3">
      <InputGroup className="mb-3">
        <FormControl
          autoFocus
          className="form-control"
          placeholder="Введите сообщение"
          value={message}
          onChange={handleChange}
          required
        />
        <Button variant="primary" type="submit">Button</Button>
      </InputGroup>
    </Form.Group>
  </Form>
  )
}