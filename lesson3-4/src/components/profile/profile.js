import React from 'react'
import {Col, Form} from 'react-bootstrap';
import ProfileInfo from "../ProfileInfo";

export default function Profile(props) {
  const { age = 0, name = 'Unknown',isOnline = true, onChangeProfileIsOnline } = props

  const handleIsOnlineChange = (event) => {
    onChangeProfileIsOnline(event.target.checked)
  }

  return (
    <Col md={{span: 4}}>
      <div>
        <p>Profile page</p>
        <ProfileInfo name={name} age={age} />

        <Form>
          <Form.Group className="mb-3" controlId="formBasicCheckbox">
            <Form.Check type="checkbox"
                        label="Online?"
                        checked={isOnline}
                        onChange={handleIsOnlineChange}
                        name="checkedB"
                        color="primary"/>
          </Form.Group>
          <Form.Label>Is user {isOnline ? 'online' : 'offline'}</Form.Label>
        </Form>
      </div>
    </Col>
  )
}