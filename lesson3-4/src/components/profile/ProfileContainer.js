import { profileSelector } from '../../selectors/profile'
import { bindActionCreators } from 'redux'
import { changeIsOnlineWithThunk } from '../../actions/profile'
import { connect } from 'react-redux'
import Profile from './profile'

const mapStateToProps = (globalState) => {
  return profileSelector(globalState)
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onChangeProfileIsOnline: changeIsOnlineWithThunk,
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Profile)