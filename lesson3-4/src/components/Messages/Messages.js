import React from "react";

export  default function Messages(props) {
  return (
    <React.Fragment>
      <p>{props.id} - {props.author}: {props.text}</p>
    </React.Fragment>
  );
}