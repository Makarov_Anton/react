import React from 'react';
import {ListGroup, Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {fetchNews, setNewsList} from "../../actions/newsAction";
import {NEWS_REQUEST_STATUS} from "../Constants/Constants";

const News = (props) => {

  const {status, list} = useSelector(state => state.news)
  const dispatch = useDispatch()

  const loadData = () => dispatch(fetchNews())
  const clearData = () => dispatch(setNewsList([]))

  if (status === NEWS_REQUEST_STATUS.LOADING) {
    return <p>LOADING...</p>
  }

  return (
    <div>
      <Button onClick={loadData} style={{marginRight: 30}}>Загрузить данные</Button>
      <Button onClick={clearData}>Очистить данные</Button>
      {status !== NEWS_REQUEST_STATUS.ERROR ?
        list.length ?
          <ListGroup style={{marginTop: 30}}as="ul" className="text-start border border border-primary d-flex justify-content-between">
            {list.map((item) => {
              return (
                <ListGroup.Item as="li" className="d-flex justify-content-between align-items-start" key={item.id}>
                  <div>{item.id}: {item.title}</div>
                </ListGroup.Item>
              )
            })}
          </ListGroup> : null
        : <p>!!!ERROR!!!</p>}
    </div>
  )
};

export default News;