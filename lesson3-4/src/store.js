import { applyMiddleware, combineReducers, createStore, compose } from 'redux'
import thunk from 'redux-thunk'
import chatsReducer from "./reducers/chats";
import profileReducer from "./reducers/profile";
import messagesReducer from "./reducers/messages";
import newsReducer from "./reducers/newsReducer";
import inputReducer from "./reducers/input";

const rootReducer = combineReducers({
  chats: chatsReducer,
  profile: profileReducer,
  messages: messagesReducer,
  news: newsReducer,
  input: inputReducer,
})

const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(
  rootReducer, composeEnchancers(applyMiddleware(thunk))
)